CREATE USER 'api'@'localhost' IDENTIFIED BY '#Passer123';
CREATE DATABASE `garage`;
GRANT SELECT, INSERT, DROP, UPDATE, CREATE, REFERENCES, INDEX, ALTER, DELETE ON garage.* TO `api`@`localhost`;

Use garage;
CREATE TABLE IF NOT EXISTS `garage_management_commune` (
  `nom` varchar(100) primary key,
  `nbClients` integer(6) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `garage_management_client` (
  `id` Integer(6) primary key AUTO_INCREMENT,
  `nom` varchar(60) DEFAULT NULL,
  `prenom` varchar(60) DEFAULT NULL,
  `adresse_id` varchar(100) DEFAULT NULL,
  CONSTRAINT `fk_Client_1` FOREIGN KEY (`adresse_id`) REFERENCES `garage_management_commune` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


Create table IF NOT EXISTS garage_management_voiture (
    immatriculation varchar(11) primary key,
    marque varchar(32),
    modele varchar(32),
    annee integer(11),
    kilometrage integer(11),
    dateArrive date,
    proprietaire_id integer(6),
    Constraint fk_keyVoiture foreign key (proprietaire_id) references garage_management_client(id)
)engine = InnoDB;

    
Create table IF NOT EXISTS garage_management_technicien (
    id integer(11) primary key auto_increment,
    nom varchar(32),
    prenom varchar(32),
    nbRep integer(11)
)engine = InnoDB;




CREATE TABLE IF NOT EXISTS `garage_management_forfait` (
  `id` integer(6) primary key AUTO_INCREMENT,
  `prix` integer(6) DEFAULT NULL,
  `nom` varchar(40) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;



Create table IF NOT EXISTS garage_management_commande(
    id integer(11) primary key auto_increment,
    statut varchar(32),
    prix integer(11),
    voiture_id varchar(11),
    forfait_id integer(6),
    technicien_id integer(11),
    constraint fk_keyCom foreign key (voiture_id) REFERENCES garage_management_voiture(immatriculation),
    constraint fk_keyForffait foreign key (forfait_id) references garage_management_forfait(id),
    constraint fk_keyTech foreign key (technicien_id) references garage_management_technicien(id)
)engine = InnoDB;



CREATE TABLE IF NOT EXISTS `garage_management_remarques` (
  `id` Integer(6) primary key AUTO_INCREMENT,
  `contenu` varchar(300) DEFAULT NULL,
  `idCommande_id` integer(11) DEFAULT NULL,
  CONSTRAINT `fk_Rem_1` FOREIGN KEY (`idCommande_id`) REFERENCES  `garage_management_commande` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


insert into garage_management_forfait values (1, 0, 'Devis sur place', 'Le client doit payer en fonction des services qui lui seront effectués');
insert into garage_management_forfait values (2, 1500, 'Forfait complet', 'Ce forfait couvre tous les frais de la voiture');
insert into garage_management_commune VALUES ('Ecully', 0);
insert into garage_management_commune VALUES ('Venissieux', 0);
insert into garage_management_client values (1,'Ndiaye', 'Mafall','Ecully');
insert into garage_management_client values (2,'Dhl', 'Donatien','Ecully');
insert into garage_management_voiture values ('DK1234AB', 'Audi', 'Q7', 2007, 1500, '2019-01-01', 1);
insert into garage_management_voiture values ('FR1234AB', 'Mercedes', 'ML350', 2009, 1000, '2019-02-02', 2);
insert into garage_management_technicien values (1,'Techos', 'Christophe', 0);
insert into garage_management_commande values (1, 'A traiter', 0, 'DK1234AB',1, 1);
insert into garage_management_commande values (2, 'A traiter', 1500, 'FR1234AB',2, 1);
insert into garage_management_remarques values (1, 'Voiture à revoir', 1);
insert into garage_management_remarques values (2, 'Voiture prête', 2);
