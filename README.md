# garage
## Deploy
```bash
virtualenv ./
source bin/activate
pip install -r requirements.txt
mysql -u root -p < init.sql
cd garage_app
python manage.py migrate
python manage.py runserver
```