from django.apps import AppConfig


class GarageManagementConfig(AppConfig):
    name = 'garage_management'
