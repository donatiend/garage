from django.contrib import admin
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

import datetime
from .models import *

from django import forms    
from .forms import *

admin.site.register(Commune)
admin.site.register(Client)
admin.site.register(Voiture)
admin.site.register(Commande)
admin.site.register(Forfait) 

def administration(request):
    if request.method == 'POST':
        form = CommandeForm(request.POST)
        if form.is_valid():
            cmd = Commande.objects.get(id=request.POST.get('cpid',''))
            cmd.statut=form.cleaned_data['statut']
            cmd.prix=form.cleaned_data['prix']
            cmd.technicien=form.cleaned_data['technicien']
            cmd.save()
            return HttpResponseRedirect('./')
        else:
            print("Form non valid")
            print(form.errors)

        
    else:
        _forms = []
        for commande in Commande.objects.all():
            _forms.append(CommandeForm(instance=commande))
            frm = CommandeForm(instance=commande)
        context = {
            'forms' : _forms
        }
        return render(request, 'Administration.html',context)
    
def accueil(request):
    return render(request, 'index.html')
    
def technicien(request):
    try:
        name = request.GET.get('name')
        surname = request.GET.get('surname')
        matricule = request.GET.get('matricule')
        if len(Technicien.objects.filter(nom=name,prenom=surname,id=matricule)) < 1:
                return HttpResponse("<marquee><strong>On ne vous connait pas</strong></marquee></br><button><a href='/authentification'>Retour a la page d'accueil</a></button>")
        technicien = Technicien.objects.filter(nom=name,prenom=surname,id=matricule)[0]
        commandes = Commande.objects.filter(technicien=technicien)
        context = {
            'commandes' : [ x.id for x in commandes],
            'technicien': technicien
        }
        return render(request, 'technicien.html', context)
    except ValueError:
        return render(request, 'authentification.html')
    
def suiviReserv(request):
    if request.method == 'POST':
        immatriculation = request.POST.get('immatriculation')
        commande = Commande.objects.filter(voiture=immatriculation)[0]
        context = {
            'commande': commande
        }
        return render(request, 'suiviReser.html', context)
    else:
        form = CheckResForm()
        return render(request, 'suiviReser.html', {'form': form})

def reservation(request):
    if request.method == 'POST' :
        form = ReservationForm(request.POST)

        if form.is_valid():
            com = Commune(nom=form.cleaned_data['your_address'], nbClients=0)
            cli = Client(nom=form.cleaned_data['your_name'], prenom=form.cleaned_data['your_surname'], adresse=com) 
            cli.save()
            voi = Voiture(immatriculation=form.cleaned_data['your_immatriculation'], marque=form.cleaned_data['your_brand'], modele=form.cleaned_data['your_model'], annee=int(form.cleaned_data['your_cdate']), kilometrage=form.cleaned_data['your_kms'], dateArrive=form.cleaned_data['your_date'], proprietaire=cli)
            voi.save()
            cmd = Commande(statut="A traiter", prix=Forfait.objects.filter(id=form.cleaned_data['your_forfait'])[0].prix, voiture=voi, forfait=Forfait.objects.filter(id=form.cleaned_data['your_forfait'])[0], technicien=None)
            cmd.save()
            return HttpResponseRedirect('/')
    else :
        form = ReservationForm()
    return render(request, 'reservation.html', {'form': form})

def identification(request):
    return render(request, 'identification.html')

def creationForf(request):
    if request.method == 'POST' :
        form = ajoutForf(request.POST)

        if form.is_valid():
            forf = Forfait(prix=form.cleaned_data['prix'],nom=form.cleaned_data['nom'],description=form.cleaned_data['description'])
            forf.save()
            return HttpResponseRedirect('../administration/')
        else :
            return HttpResponse("<marquee><strong>Ce forfait n'est pas valide</strong></marquee></br><button><a href='../administration/'>Retour a la page d'administrateur</a></button>")
    else :
        form = ajoutForf(request.POST)
    return render(request, 'ajoutForfait.html', locals())

def creationTech(request):
    if request.method == 'POST' :
        form = ajoutTech(request.POST)
        if form.is_valid():
            tech = Technicien(nom=form.cleaned_data['nom'],prenom=form.cleaned_data['prenom'],nbRep = 0)
            tech.save()
            return HttpResponseRedirect('../administration/')
        else :
            return HttpResponse("<marquee><strong>Ce format de technicien n'est pas valide</strong></marquee></br><button><a href='../administration/'>Retour a la page d'administrateur</a></button>")
    else :
        form = ajoutTech(request.POST)
    return render(request, 'ajoutTech.html', locals())

def listerTechnicien(request):
    if request.method == 'POST' :
        Technicien.objects.filter(id=request.POST.get('id')).delete()
    techniciens = Technicien.objects.all()
    context = {
        'techniciens': techniciens
    }
    return render(request, 'listTech.html',context)

def authentification(request):
    return render(request, 'authentification.html')

def ajoutConstat(request):
    if request.method == 'POST' :
        name = request.POST.get('name')
        surname = request.POST.get('surname')
        matricule = request.POST.get('matricule')
        form = RemarqueTech(request.POST)
        if form.is_valid():
            if len(Commande.objects.filter(id = form.cleaned_data['commande'])) > 1 :
                constat = Remarques(idCommande=Commande.objects.filter(id = form.cleaned_data['commande'])[0] ,contenu=form.cleaned_data['Constat'])
                constat.save()
                return HttpResponse(f"<marquee><strong>Constat enregistré</strong></marquee></br><button><a href='/technicien?name={name}&surname={surname}&matricule={matricule}'>Retour a la page précédente</a></button>")
            else : 
                return HttpResponse(f"<marquee><strong>Cette commande ne vous est pas attribuée</strong></marquee></br><button><a href='/technicien?name={name}&surname={surname}&matricule={matricule}'>Retour a la page précédente</a></button>")
        else :
            return HttpResponse("<marquee><strong>Ce constat n'est pas valide</strong></marquee></br><button><a href='../administration/'>Retour a la page d'administrateur</a></button>")
    return HttpResponse("<marquee><strong>Ce constat n'est pas valide</strong></marquee></br><button><a href='../administration/'>Retour a la page d'administrateur</a></button>")
    # return render(request, '.html', locals())

def listRemarque(request):
    remarques = Remarques.objects.all()
    context = {
        'remarques': remarques
    }
    return render(request, 'listRemarque.html',context)

def listForfait(request):
    if request.method == 'POST' :
        Forfait.objects.filter(id=request.POST.get('id')).delete()
    forfaits = Forfait.objects.all()
    context = {
        'forfaits': forfaits
    }
    return render(request, 'listForfait.html',context)
