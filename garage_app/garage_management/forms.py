from django import forms
from django.core.validators import MaxValueValidator

from .models import Forfait, Commande, Voiture

class ReservationForm(forms.Form):
    forfaits = tuple((f.id, f"{f.nom} - {f.prix} €" ) for f in Forfait.objects.all())
    your_name = forms.CharField(label='Nom', max_length=60)
    your_surname = forms.CharField(label='Prénom', max_length=60)
    your_address = forms.CharField(label='Adresse', max_length=100)
    your_immatriculation = forms.CharField(label='Immatriculation voiture', max_length=11)
    your_kms = forms.IntegerField(label='Kilométrage véhicule', validators=[MaxValueValidator(1e7)])
    your_cdate = forms.IntegerField(label='Année véhicule', validators=[MaxValueValidator(2020)])
    your_brand = forms.CharField(label='Marque', max_length=32)
    your_model = forms.CharField(label='Modèle', max_length=32)
    your_forfait = forms.ChoiceField(choices = (('', '')), label="Forfait", initial='', widget=forms.Select(), required=True)
    your_date = forms.DateField(label='Date')

    def __init__(self, *args, **kwargs):
        super(ReservationForm, self).__init__(*args, **kwargs)

        # Load choices here so db calls are not made during migrations.
        self.fields['your_forfait'].choices = tuple((f.id, f"{f.nom} - {f.prix} €" ) for f in Forfait.objects.all())

class CommandeForm(forms.ModelForm):
    class Meta:
        model = Commande
        fields = ['id', 'statut', 'prix', 'technicien']

    def __init__(self, *args, **kwargs):
        super(CommandeForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs :
            _id = kwargs['instance'].id
            _voiture = kwargs['instance'].voiture
            _forfait = kwargs['instance'].forfait
            self.fields['immatriculation']=forms.CharField(initial=_voiture.immatriculation , disabled=True)
            self.fields['propname']=forms.CharField(initial=f"{_voiture.proprietaire.prenom}  {_voiture.proprietaire.nom}", disabled=True)
            self.fields['datearv']=forms.DateField(initial=_voiture.dateArrive , disabled=True)
            self.fields['frf']=forms.DateField(initial=_forfait.nom , disabled=True)
            self.fields['cpid']=forms.IntegerField(initial=_id, widget=forms.HiddenInput())

class ajoutForf(forms.Form):
    nom = forms.CharField(label='nom', max_length=40)
    description = forms.CharField(label='description', max_length=300)
    prix = forms.IntegerField(label='prix')

class ajoutTech(forms.Form):
    nom = forms.CharField(label='nom', max_length=40)
    prenom = forms.CharField(label='prenom', max_length=40)

class CheckResForm(forms.Form):
    immatriculation = forms.CharField(label="Immatriculation", max_length=11)

class RemarqueTech(forms.Form):
    commande = forms.CharField(label='commande', max_length=40)
    Constat = forms.CharField(label='Constat', max_length=300)
