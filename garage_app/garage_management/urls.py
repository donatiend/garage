from django.urls import path

from . import views

urlpatterns = [
    path('', views.accueil),
    path('administration/', views.administration),
    path('technicien/', views.technicien),
    path('suiviReser/', views.suiviReserv),
    path('reservation/', views.reservation),
    path('connexion/', views.identification),
    path('ajoutForfait/', views.creationForf),
    path('ajoutTech/', views.creationTech),
    path('listTech/', views.listerTechnicien),
    path('authentification/', views.authentification),
    path('ajoutconstat/', views.ajoutConstat),
    path('listRemarque/', views.listRemarque),
    path('listForfait/', views.listForfait),
]
