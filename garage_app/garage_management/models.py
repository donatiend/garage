from django.db import models

# Create your models here.
class Commune(models.Model):
    nom = models.CharField(max_length=100, primary_key=True)
    nbClients = models.IntegerField(default=0)

class Client(models.Model):
    nom = models.CharField(max_length=60)
    prenom = models.CharField(max_length=60)
    adresse = models.ForeignKey(Commune, on_delete=models.CASCADE)
    
    def save(self, *args, **kwargs):
        adr, _ = Commune.objects.get_or_create(nom=self.adresse.nom)
        adr.nbClients += 1
        adr.save()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.nom

class Voiture(models.Model):
    immatriculation = models.CharField(max_length=11, primary_key=True)
    marque = models.CharField(max_length=32)
    modele = models.CharField(max_length=32)
    annee = models.IntegerField()
    kilometrage = models.IntegerField()
    dateArrive = models.DateField()
    proprietaire = models.ForeignKey(Client, on_delete=models.CASCADE)

class Technicien(models.Model):
    nom = models.CharField(max_length=32)
    prenom = models.CharField(max_length=32)
    nbRep = models.IntegerField()
    

class Forfait(models.Model):
    prix = models.IntegerField()
    nom = models.CharField(max_length=40)
    description = models.CharField(max_length=300)

class Commande(models.Model):
    statut = models.CharField(max_length=32)
    prix = models.IntegerField()
    voiture = models.ForeignKey(Voiture, on_delete=models.CASCADE)
    forfait = models.ForeignKey(Forfait, on_delete=models.CASCADE)
    technicien = models.ForeignKey(Technicien, on_delete=models.CASCADE, blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.technicien != None :
            print(self.technicien.id)
            print(Technicien.objects.filter(id=self.technicien.id)[0])
            tech = Technicien.objects.filter(id=self.technicien.id)[0]
            tech.nbRep += 1
            tech.save()
        super().save(*args, **kwargs)

class Remarques(models.Model):
    contenu = models.CharField(max_length=300)
    idCommande = models.ForeignKey(Commande, on_delete=models.CASCADE)
